import { Injectable } from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs'

@Injectable({
  providedIn: 'root'
})
export class CurrentProductService {

  private currentProduct$ = new BehaviorSubject('')

  get(): Observable<string> {
    return this.currentProduct$
  }

  set(product: string) {
    this.currentProduct$.next(product)
  }
}
