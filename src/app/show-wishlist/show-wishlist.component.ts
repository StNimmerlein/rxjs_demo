import {ChangeDetectorRef, Component} from '@angular/core'
import {UserService} from '../user.service'
import {WishlistService} from '../wishlist.service'
import {pluck, switchMap, take} from 'rxjs/operators'

@Component({
  selector: 'app-show-wishlist',
  templateUrl: './show-wishlist.component.html',
  styleUrls: ['./show-wishlist.component.css']
})
export class ShowWishlistComponent {

  wishlist: string[] = []

  constructor(
    private userService: UserService,
    private wishlistService: WishlistService,
    private cd: ChangeDetectorRef
  ) {
  }

  fetchWishlist() {
    this.userService.getActive().pipe(
      pluck('username'),
      switchMap(username => this.wishlistService.getWishlist(username)),
      take(1)
    ).subscribe(wishlist => {
      this.wishlist = wishlist
      this.cd.detectChanges()
    })
  }

}
