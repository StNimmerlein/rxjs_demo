import {ChangeDetectorRef, Component, OnDestroy, OnInit} from '@angular/core'
import {UserService} from '../user.service'
import {pluck} from 'rxjs/operators'
import {Subscription} from 'rxjs'

@Component({
  selector: 'app-user-selector',
  templateUrl: './user-selector.component.html',
  styleUrls: ['./user-selector.component.css']
})
export class UserSelectorComponent implements OnInit, OnDestroy {

  usernames$ = this.userService.getUsernames()
  activeUsername: string | undefined

  private subscriptions = new Subscription()

  constructor(private userService: UserService, private cd: ChangeDetectorRef) {
  }

  ngOnInit() {
    this.subscriptions.add(
      this.userService.getActive().pipe(pluck('username')).subscribe(username => {
        this.activeUsername = username
        this.cd.detectChanges()
      })
    )
  }

  ngOnDestroy() {
    this.subscriptions.unsubscribe()
  }

  logout() {
    this.userService.logout()
  }

  login(event: Event) {
    this.userService.login((event.target as any).value)
  }

}
