import {Injectable} from '@angular/core'
import {BehaviorSubject, from, Observable, of} from 'rxjs'
import {anonymous, User, users} from './user-repo'
import {filter, map, pluck, take} from 'rxjs/operators'

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private activeUser$ = new BehaviorSubject<User>(anonymous)

  constructor() {
  }

  getUsernames(): Observable<string[]> {
    return of(users).pipe(map(users => users.map(user => user.username)))
  }

  getActive(): Observable<User> {
    return this.activeUser$
  }

  login(username: string) {
    let user = users.find(user => user.username === username)

    if (user) {
      this.activeUser$.next(user)
    }
  }

  logout() {
    this.activeUser$.pipe(
      take(1),
      filter(user => user != anonymous)
    ).subscribe(() =>
      this.activeUser$.next(anonymous)
    )
  }
}
