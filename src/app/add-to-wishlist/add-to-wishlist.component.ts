import { Component, OnInit } from '@angular/core';
import {CurrentProductService} from '../current-product.service'
import {UserService} from '../user.service'
import {combineLatest} from 'rxjs'
import {take} from 'rxjs/operators'
import {WishlistService} from '../wishlist.service'

@Component({
  selector: 'app-add-to-wishlist',
  templateUrl: './add-to-wishlist.component.html',
  styleUrls: ['./add-to-wishlist.component.css']
})
export class AddToWishlistComponent {

  constructor(
    private currentProductService: CurrentProductService,
    private userService: UserService,
    private wishlistService: WishlistService
  ) { }

  addToWishlist() {
    combineLatest([
      this.currentProductService.get(),
      this.userService.getActive()
    ]).pipe(
      take(1)
    ).subscribe(([product, user]) => {
      this.wishlistService.addToWishlist(user.username, product)
    })
  }
}
