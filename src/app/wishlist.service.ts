import {Injectable} from '@angular/core'
import {BehaviorSubject, Observable, of, Subject} from 'rxjs'

@Injectable({
  providedIn: 'root'
})
export class WishlistService {
  private wishlist: {
    [username: string]: string[]
  }
  private localStorage

  // private subjects: {
  //   [username: string]: Subject<string[]>
  // } = {}

  constructor(window: Window) {
    this.localStorage = window.localStorage
    let storedWishlist = this.localStorage.getItem('wishlist')
    this.wishlist = storedWishlist ? JSON.parse(storedWishlist) : new Map<string, string[]>()
  }

  addToWishlist(username: string, product: string) {
    alert(`Added product "${product}" to wishlist of user "${username}"`)

    // this.wishlist[username] = (this.wishlist[username] ?? []).concat(product)
    // this.subjects[username]?.next(this.wishlist[username])

    this.localStorage.setItem('wishlist', JSON.stringify(this.wishlist))
  }

  getWishlist(username: string): Observable<string[]> {
    return of(this.wishlist[username] ?? [])
  }

  // getWishlist(username: string): Observable<string[]> {
  //   this.subjects[username] ??= new BehaviorSubject(this.wishlist[username])
  //   return this.subjects[username]
  // }
}
