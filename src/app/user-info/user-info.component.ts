import {ChangeDetectorRef, Component, OnDestroy, OnInit} from '@angular/core'
import {Subscription} from 'rxjs'
import {UserService} from '../user.service'
import {User} from '../user-repo'

@Component({
  selector: 'app-user-info',
  templateUrl: './user-info.component.html',
  styleUrls: ['./user-info.component.css']
})
export class UserInfoComponent implements OnInit, OnDestroy {
  activeUser: User | undefined

  private subscriptions = new Subscription()

  constructor(
    private userService: UserService,
    private cd: ChangeDetectorRef
  ) {
  }

  ngOnInit() {
    this.subscriptions.add(
      this.userService.getActive().subscribe(activeUser => {
        this.activeUser = activeUser
        this.cd.detectChanges()
      })
    )
  }

  ngOnDestroy() {
    this.subscriptions.unsubscribe()
  }

}
