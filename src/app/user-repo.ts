export type User = {
  username: string,
  description: string,
  status: 'online' | 'offline',
  age: number,
  favoriteAnimal: string
}

export const users: User[] = [
  {
    username: 'Haxxor1337',
    age: 15,
    description: 'y0u d0n\'t und3rst4nd me! n00b!',
    favoriteAnimal: 'Pikachu',
    status: 'online'
  },
  {
    username: 'Gönnhart',
    age: 33,
    status: 'offline',
    favoriteAnimal: 'Mittwochsfrosch',
    description: 'Erst mal ne Chill-Pill...'
  },
  {
    username: 'Gönnherta',
    age: 75,
    status: 'online',
    favoriteAnimal: 'Dönertier',
    description: '75, aber kann mit dir immer noch den Boden aufwischen #granny4life'
  },
  {
    username: 'BallsOfSteel',
    age: 13,
    description: 'Ich bin zwar erst dreizehn aber schon viel weiter als die anderen Kids',
    favoriteAnimal: 'Flunder',
    status: 'offline'
  }
]

export const anonymous: User = {
  username: 'anonymous',
  status: 'online',
  favoriteAnimal: '',
  description: 'Unregistered user',
  age: NaN
}
