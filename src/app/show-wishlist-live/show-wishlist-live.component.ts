import {Component, OnDestroy, OnInit} from '@angular/core'
import {UserService} from '../user.service'
import {WishlistService} from '../wishlist.service'
import {Observable, Subscription} from 'rxjs'
import {pluck, switchMap} from 'rxjs/operators'

@Component({
  selector: 'app-show-wishlist-live',
  templateUrl: './show-wishlist-live.component.html',
  styleUrls: ['./show-wishlist-live.component.css']
})
export class ShowWishlistLiveComponent implements OnInit {

  wishlist$: Observable<string[]> | undefined

  constructor(
    private userService: UserService,
    private wishlistService: WishlistService,
  ) { }

  ngOnInit(): void {
    this.wishlist$ = this.userService.getActive().pipe(
      pluck('username'),
      switchMap(username => this.wishlistService.getWishlist(username))
    )
  }

}
