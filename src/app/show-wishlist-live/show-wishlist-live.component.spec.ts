import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowWishlistLiveComponent } from './show-wishlist-live.component';

describe('ShowWishlistLiveComponent', () => {
  let component: ShowWishlistLiveComponent;
  let fixture: ComponentFixture<ShowWishlistLiveComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ShowWishlistLiveComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowWishlistLiveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
