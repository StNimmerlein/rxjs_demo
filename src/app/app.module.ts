import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { UserInfoComponent } from './user-info/user-info.component';
import { UserSelectorComponent } from './user-selector/user-selector.component';
import { ProductSelectorComponent } from './product-selector/product-selector.component';
import { AddToWishlistComponent } from './add-to-wishlist/add-to-wishlist.component';
import { ShowWishlistComponent } from './show-wishlist/show-wishlist.component';
import { ShowWishlistLiveComponent } from './show-wishlist-live/show-wishlist-live.component';

@NgModule({
  declarations: [
    AppComponent,
    UserInfoComponent,
    UserSelectorComponent,
    ProductSelectorComponent,
    AddToWishlistComponent,
    ShowWishlistComponent,
    ShowWishlistLiveComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [{
    provide: Window, useValue: window
  }],
  bootstrap: [AppComponent]
})
export class AppModule { }
