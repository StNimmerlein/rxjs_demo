import {Component, ElementRef, OnInit, ViewChild} from '@angular/core'
import {CurrentProductService} from '../current-product.service'

@Component({
  selector: 'app-product-selector',
  templateUrl: './product-selector.component.html',
  styleUrls: ['./product-selector.component.css']
})
export class ProductSelectorComponent {

  @ViewChild('productName')
  private input: ElementRef<HTMLInputElement> | undefined

  constructor(
    private currentProductService: CurrentProductService
  ) { }

  setProduct() {
    const productName = this.input?.nativeElement?.value ?? ''
    this.currentProductService.set(productName)
  }
}
